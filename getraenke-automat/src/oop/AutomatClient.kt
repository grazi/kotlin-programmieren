package oop

import oop.automat.Automat
import oop.getraenk.Getraenk

fun main(args: Array<String>) {

    val getraenkeAutomat = Automat()

    getraenkeAutomat.nimmGetraenkAuf(Getraenk(1, "San Pellegrino", 0.50))
    getraenkeAutomat.nimmGetraenkAuf(Getraenk(2, "Fanta", 1.00))
    getraenkeAutomat.nimmGetraenkAuf(Getraenk(3, "Luzerner Bier",2.00))
    getraenkeAutomat.nimmGetraenkAuf(Getraenk(4, "Coca Cola",5.00))

    getraenkeAutomat.geheInBetrieb()

}