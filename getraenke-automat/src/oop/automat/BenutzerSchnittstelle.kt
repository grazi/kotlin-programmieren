package oop.automat

import oop.getraenk.Getraenk

class BenutzerSchnittstelle(val getaenkKatalog: ArrayList<Getraenk>) {

    fun liesGetraenkEin(): Getraenk {
        val getraenkeNr = liesZahlEin(blendeGetraenkeAuswahlEin())
        for (getraenk in getaenkKatalog) {
            if (getraenkeNr == getraenk.id) {
                return getraenk
            }
        }
        throw IllegalStateException("Fehler: Kein Getränk ($getraenkeNr) gefunden!")
    }

    fun getraenkAusgeben(getraenk: Getraenk) {
        println("Vielen Dank, bitte entnehmen sie ihr Getränk (${getraenk.name}).")
    }

    fun geldZurueckgeben(getraenk: Getraenk) {
        println("Es gibt noch einen Rest von ${getraenk.gibRestBetrag()} CHF!")
    }

    fun forderePreisEin(getraenk: Getraenk): Double {
        return liesFliessKommaZahlEin(betragEingebenFrage(getraenk))
    }

    private fun blendeGetraenkeAuswahlEin(): String {
        val getraenkeAuswahl = StringBuilder()
                .appendln("Wählen sie ihr Getränk aus:")
                .appendln()

        for (getraenk in getaenkKatalog) {
            getraenkeAuswahl.appendln("${getraenk.id}) ${getraenk.name} (${getraenk.preis})")
        }

        getraenkeAuswahl.appendln("Geben Sie die Getränke Nummer ein:")

        return getraenkeAuswahl.toString()
    }

    private fun liesZahlEin(frage: String): Int {

        var zahl: Int? = null
        do {
            println(frage)
            zahl = readLine()?.toIntOrNull()
        } while (zahl == null)

        return zahl
    }

    private fun liesFliessKommaZahlEin(frage: String): Double {

        var fliesskommaZahl: Double? = null
        do {
            println(frage)

            fliesskommaZahl = readLine()?.toDoubleOrNull()

        } while (fliesskommaZahl == null)

        return fliesskommaZahl
    }

    private fun betragEingebenFrage(getraenk: Getraenk): String {
        return "Bitte werfen Sie für die ${getraenk.name} bitte ${getraenk.betrag} CHF ein"
    }

}