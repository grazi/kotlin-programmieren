package oop.automat

import oop.getraenk.Getraenk

class Automat {

    val getraenkeKatalog = arrayListOf<Getraenk>()
    val schnittstelle = BenutzerSchnittstelle(getraenkeKatalog)

    fun nimmGetraenkAuf(getraenk: Getraenk) {
        if (!getraenkeKatalog.contains(getraenk)) {
            getraenkeKatalog.add(getraenk)
        }
    }

    fun geheInBetrieb() {
        while (true) {
            val getraenk = schnittstelle.liesGetraenkEin()
            getraenkBezahlen(getraenk)
            schnittstelle.getraenkAusgeben(getraenk)
            if (getraenk.gibtEsGeldZurueck()) {
                schnittstelle.geldZurueckgeben(getraenk)
            }
        }
    }

    private fun getraenkBezahlen(getraenk: Getraenk) {
        do {
            val einbezahlterBetrag = schnittstelle.forderePreisEin(getraenk)
            getraenk.zieheBetragAb(einbezahlterBetrag)
        } while (getraenk.istWeitereZahlungNoetig())
    }
}