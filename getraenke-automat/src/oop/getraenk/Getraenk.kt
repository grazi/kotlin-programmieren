package oop.getraenk

import java.math.BigDecimal

class Getraenk(val id:Int, val name:String, val preis:Double) {

    var betrag:BigDecimal = BigDecimal.valueOf(preis)
        private set

    fun istWeitereZahlungNoetig(): Boolean {
        return betrag > BigDecimal("0")
    }

    fun zieheBetragAb(einbezahlterBetrag: Double) {
        if (einbezahlterBetrag < 0) {
            throw IllegalStateException("Fehler: Minus Betrag: $einbezahlterBetrag nicht erlaubt!")
        }

        betrag = betrag.subtract(BigDecimal(einbezahlterBetrag))
        betrag = betrag.setScale(2, BigDecimal.ROUND_HALF_EVEN)
    }

    fun gibtEsGeldZurueck(): Boolean {
        return betrag < BigDecimal.ZERO
    }

    fun gibRestBetrag(): Double {
        if (betrag >= BigDecimal.ZERO) {
            return 0.0;
        }

        return betrag.multiply(BigDecimal("-1")).toDouble()
    }


}