package procedural

fun main(args: Array<String>) {
    val getraenke = hashMapOf(
            Pair(1, 0.50),
            Pair(2, 1.00),
            Pair(3, 2.00),
            Pair(4, 3.00))

    while (true) {
        val preis = liesGetraenkEin(getraenke)
        fordereBetragEin(preis)
    }

}

fun liesGetraenkEin(getraenke: HashMap<Int, Double>): Double {

    var getraenkNr: Int?

    do {
        getraenkNr = liesZahlEin(getraenkeAuswahlFrage())
    } while (!getraenke.contains(getraenkNr))


    val preis = getraenke[getraenkNr]!!

    return preis
}

fun rundeBetrag(betrag: Double): Double {
    return Math.round(betrag * 100.0) / 100.0
}

fun betragEingebenFrage(preis: Double): String {
    return "Bitte werfen Sie $preis CHF ein"
}

fun fordereBetragEin(betrag: Double) {
    val eingegebenerBetrag = liesFliessKommaZahlEin(betragEingebenFrage(betrag))

    when (betrag.compareTo(eingegebenerBetrag)) {
        -1 -> {
            val restBetrag = rundeBetrag(eingegebenerBetrag - betrag)
            println("Vielen Dank, bitte entnehmen sie ihr Getränk und den Rest von $restBetrag CHF")
        }

        0 -> println("Vielen Dank, bitte entnehmen sie ihr Getränk.")

        1 -> {
            val fehlenderBetrag = rundeBetrag(betrag - eingegebenerBetrag)
            println("Zu wenig Geld eingeworfen, bitt zahlen sich noch weitere $fehlenderBetrag CHF ein!")
            fordereBetragEin(fehlenderBetrag)
        }
    }
}

fun liesFliessKommaZahlEin(frage: String): Double {

    var fliesskommaZahl: Double? = null
    do {
        println(frage)

        fliesskommaZahl = readLine()?.toDoubleOrNull()

    } while (fliesskommaZahl == null)

    return fliesskommaZahl
}

fun liesZahlEin(frage: String): Int {

    var zahl: Int?
    do {
        println(frage)

        zahl = readLine()?.toIntOrNull()

    } while (zahl == null)

    return zahl
}

fun getraenkeAuswahlFrage(): String {
    return """Wählen sie ihr Getränk aus:
    |
    |      1) Wasser (0,50 CHF)
    |      2) Limonade (1,00 CHF)
    |      3) Bier (2,00 CHF)
    |      4) Cola (3.00 CHF)
    |
    |      Geben sie 1, 2, 3 oder 4 ein:
    |""".trimMargin()
}