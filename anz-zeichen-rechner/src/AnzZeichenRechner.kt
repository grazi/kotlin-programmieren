fun main(args: Array<String>) {

    val text = frageTextAb("Bitte gib einen Text ein:")

    val anzZeichen = berechneTextLaenge(text)

    gibAnzZeichenAus(text, anzZeichen)

}

fun gibAnzZeichenAus(text:String, anzZeichen: Int) {

    println("Der Text:\n\n$text\n\n..besteht aus $anzZeichen Zeichen.")
}

fun  berechneTextLaenge(text: String): Int {

    return text.length
}

fun frageTextAb(frage: String): String {

    println(frage)

    val text = readLine()

    if (text == null) {
        return ""
    }

    return text
}