fun main(args: Array<String>) {

    // variable deklarieren
    val text:String

    // Wertzuweisung
    text = "Hallo Welt"

    println(text)

    println("Hallo Welt")

    // Deklarieren und Wert zuweisen
    val begruessung = "Guten Abend"

    println(begruessung)

    // text = "Hello World" geht nicht -> da val genutzt wird -> nur eine Wertzuweisung möglich

    var hallo = "Hallo"
    println(hallo)

    hallo = "Ciao"
    println(hallo)

}