class Farbe(var name:String) {

    fun wechsleFarbe(farbenName:String) {
        name = farbenName
    }

    fun verwendeFarbe() {
        println("Die Farbe: $name wird verwendet")
    }
}