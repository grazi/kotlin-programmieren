fun main(args: Array<String>) {

    val einEtui = Etui("Pelikan")

    einEtui.legeStiftInEtui(BleiStift(Farbe("gelb")))
    einEtui.legeStiftInEtui(FilzStift(Farbe("rot")))
    einEtui.legeStiftInEtui(BleiStift(Farbe("blau")))

    for (einStift in einEtui.stifte) {
        if (einStift is FilzStift) {
            TintenFass.fuelleFilzStiftNach(einStift)
        }

    }
}
