abstract class Stift(val eineFarbe:Farbe) {

    fun verwendeStift() {
        eineFarbe.verwendeFarbe()
    }

    fun wechsleFarbe(farbeName:String) {
        eineFarbe.wechsleFarbe(farbeName)
    }

}