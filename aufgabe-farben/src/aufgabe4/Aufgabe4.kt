package aufgabe4

fun main(args: Array<String>) {

    val farben = arrayListOf<String>("Gelb", "Rot", "Blau")

    while (true) {
        fuehreOptionAus(farben)
    }
}

private fun fuehreOptionAus(farben: ArrayList<String>) {
    val option = leseOptionEin()
    if (option == 1) {
        val neueFarbe = leseNeueFarbeEin(farben)
        farben.add(neueFarbe)
    } else {
        val eingegebeneFarbe = leseFarbeEin(farben)
        println(eingegebeneFarbe)
    }
}

fun leseOptionEin(): Int {
    println("Du kannst aus folgenden Optionen wählen:")
    println("1) Weitere Farben eingeben")
    println("2) Farbe zum Ausgeben eingeben")

    var eingegebeneZahl: Int
    do {
        eingegebeneZahl = frageZahlAb()
    } while (eingegebeneZahl != 1 && eingegebeneZahl != 2)

    return eingegebeneZahl
}

fun frageZahlAb(): Int {

    var zahl: Int?
    // Schleife ist solange aktiv bis die Variable Zahl nicht mehr null ist
    do {
        println("Gib eine 1 oder eine 2 ein:")
        zahl = readLine()?.toIntOrNull()
    } while (zahl == null)

    return zahl
}

fun gibFarbenFrageAus(farben: ArrayList<String>) {
    println("Bitte geben Sie eine dieser Farben ein:")
    gibFarbenAus(farben)
}

fun leseNeueFarbeEin(farben: ArrayList<String>): String {
    println("Bitte geben Sie eine neue Frabe ein:")
    var eingegebeneFarbe = ""
    do {
        eingegebeneFarbe = readLine().toString()
    } while (farben.contains(eingegebeneFarbe))
    return eingegebeneFarbe
}

fun leseFarbeEin(farben: ArrayList<String>): String {
    var eingegebeneFarbe = ""
    do {
        gibFarbenFrageAus(farben)
        eingegebeneFarbe = readLine().toString()
    } while (!farben.contains(eingegebeneFarbe))
    return eingegebeneFarbe
}

private fun gibFarbenAus(farben: ArrayList<String>) {
    val lastIndex = farben.size - 1
    for (i in 0..lastIndex) {
        print(farben[i])
        if (i != lastIndex) {
            if (i == lastIndex - 1) {
                print(" oder ")
            } else {
                print(", ")
            }
        }
    }
    println()
}