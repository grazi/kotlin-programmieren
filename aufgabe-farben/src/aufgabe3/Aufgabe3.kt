package aufgabe3

fun main(args: Array<String>) {

    val farben = arrayListOf<String>("Gelb", "Rot", "Blau")

    val eingegebeneFarbe = leseFarbeEin(farben)

    println(eingegebeneFarbe)

}

fun gibFarbenFrageAus(farben: ArrayList<String>){
    println("Bitte geben Sie eine dieser Farben ein:")
    gibFarbenAus(farben)
}

fun leseFarbeEin(farben: ArrayList<String>): String {
    var eingegebeneFarbe = ""
    do {
        gibFarbenFrageAus(farben)
        eingegebeneFarbe = readLine().toString()
    } while (!farben.contains(eingegebeneFarbe))
    return eingegebeneFarbe
}

private fun gibFarbenAus(farben: ArrayList<String>) {
    val lastIndex = farben.size - 1
    for (i in 0..lastIndex) {
        print(farben[i])
        if (i != lastIndex) {
            if (i == lastIndex - 1) {
                print(" oder ")
            } else {
                print(", ")
            }
        }
    }
    println()
}