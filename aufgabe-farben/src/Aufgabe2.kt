fun main(args: Array<String>) {

    val farben = arrayListOf<String>("Gelb", "Rot", "Blau")

    // Variante 1
    for (farbe in farben) {
        if (farbe == "Gelb") {
            println(farbe)
        }
    }

    // Variante 2
    val farbe = farben.get(0)
    println(farbe)

}