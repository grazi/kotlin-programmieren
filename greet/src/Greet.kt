fun main(args: Array<String>) {

    println("Bitte gib Dein Vornamen ein:")

    val vorname = readLine()

    val greeting = greet("Graziano")

    println(greeting)

}


fun greet(bingo: String?): String? {

    val ausgabeText:String

    if (bingo == null || bingo == "" ) {
        ausgabeText = "Achtung, kein Vorname gesetzt!!"
    } else if (bingo.length <= 1) {
        ausgabeText = "Hey, $bingo ist echt ein cooler Name!!"
    } else if (bingo.length < 3) {
        ausgabeText = "Hey, $bingo ist kein Name!!"
    } else {
        ausgabeText = "Hallo $bingo, herzlich willkommen!"
    }

    return ausgabeText

}


