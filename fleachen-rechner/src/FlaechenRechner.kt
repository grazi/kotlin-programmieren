fun main(args: Array<String>) {

    val laenge = frageFliessKomaZahlAb("Gib bitte eine Länge in Metern an:")
    val breite = frageFliessKomaZahlAb("Gib bitte eine Breite in Metern an:")

    val flaeche = berechneFlaeche(laenge, breite)

    gibFlaecheAus(flaeche)
}

fun gibFlaecheAus(flaeche: Double) {
    println("Die Fläche beträgt: $flaeche m^2 ")
}

fun berechneFlaeche(laenge: Double, breite: Double) : Double {

    return laenge * breite
}

fun frageFliessKomaZahlAb(frage: String): Double {

    println(frage)

    val fliessKommaZahl = readLine()?.toDoubleOrNull()

    if (fliessKommaZahl == null) {
        return 0.0
    }

    return fliessKommaZahl
}
