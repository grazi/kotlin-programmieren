

object FordFabrik {

    fun baueEinAuto(modell:ModellTyp) :FordAuto? {

        var auto:FordAuto? = null

        when (modell) {
            ModellTyp.Fiesta -> auto = Fiesta()
            ModellTyp.Mondeo -> auto = Mondeo()
            else -> return null
        }

        println("... Ihr $modell wird gebaut..")
        return auto
    }
}