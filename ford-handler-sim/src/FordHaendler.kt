class FordHaendler(private val name:String) {

    fun kaufeEinAuto(modell:ModellTyp) : FordAuto? {
        println("Herzlich Willkommen bei $name")

        val auto = FordFabrik.baueEinAuto(modell)

        if (auto == null) {
            println("Sorry, das Modell: $modell können wir nicht bestellen!")
        } else {
            println("Ich werde für Sie ein $modell bestellen.")
        }




        return auto
    }
}