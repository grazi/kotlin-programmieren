fun main(args: Array<String>) {

    beispielListe()

    beispielFor()

    beispieWhile()

    // Beispiel Fussgesteurte while Schleife
    beispieWhileBenutzerFragen()

}

fun beispieWhileBenutzerFragen() {
    var zahl:Int? = null
    // Schleife ist solange aktiv bis die Variable Zahl nicht mehr null ist
    do {
        println("Gib eine Zahl ein:")
        zahl = readLine()?.toIntOrNull()
    } while (zahl == null)

    println(zahl)
}

fun beispieWhile() {
    var zahl = 2
    while (zahl < 42) {
        zahl = zahl * 2
        println(zahl)
    }
}

private fun beispielFor() {
    for (i in 1..10) {

        if (i % 2 == 0) {
            println(i)
        } else {
            println("$i ist ungerade")
        }
    }
}

private fun beispielListe() {
    val personalAusweisListe = arrayListOf<String>("id-1", "susi", "bingo")

    for (einText in personalAusweisListe) {
        println(einText)
    }

    personalAusweisListe.add("id-3333")

    println(personalAusweisListe)

    personalAusweisListe.removeAt(3)

    println(personalAusweisListe)
}

