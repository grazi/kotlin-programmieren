class FrachtFlugzeug(marke:String) : Flugzeug(marke) {

    val frachtRaum = arrayListOf<FrachtEinheit>()

    fun ladeFrachtEin(frachtEinheit: FrachtEinheit) {
        frachtRaum.add(frachtEinheit)
    }

    fun ladeFrachtAus(id:String) : FrachtEinheit? {
        for (frachtEinheit in frachtRaum) {
            if (frachtEinheit.id == id) {
                frachtRaum.remove(frachtEinheit)
                return frachtEinheit
            }
        }
        return null
    }

}