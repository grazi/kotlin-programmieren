class PassagierFlugzeug(marke:String) : Flugzeug(marke) {

    val passagiere = arrayListOf<Person>()

    fun boardingPassagier(person: Person) {
        passagiere.add(person)
    }

    fun deBoardingPassagiere() : List<Person> {
        return passagiere.toList()
    }

}