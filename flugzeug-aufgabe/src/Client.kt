fun main(args: Array<String>) {

    val frachtFlugzeug = FrachtFlugzeug("Tupolev")

    frachtFlugzeug.ladeFrachtEin(FrachtEinheit("1", "Plastik"))
    frachtFlugzeug.ladeFrachtEin(FrachtEinheit("2", "Stahl"))

    frachtFlugzeug.flyTo("Paris")

    frachtFlugzeug.ladeFrachtAus("1")
    frachtFlugzeug.ladeFrachtAus("2")

    val passagierFlugzeug = PassagierFlugzeug("Airbus A380")

    passagierFlugzeug.boardingPassagier(Person("Fritz"))
    passagierFlugzeug.boardingPassagier(Person("Max"))

    passagierFlugzeug.flyTo("Denver")

    passagierFlugzeug.deBoardingPassagiere()

}