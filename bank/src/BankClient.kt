
fun main(args: Array<String>) {

    val konti = arrayListOf<Konto>(
            Konto("11-A", "Max"),
            Konto("12-A", "Susi")
    )


    konti[0].zahleEin(100000.00)
    konti[0].zahleAus(500.00)


    for (konto in konti) {
        println("${konto.ktoNummer}, ${konto.ktoInhaber}, ${konto.gibKtoStandAus()}")
    }



    Konto("", "")


}