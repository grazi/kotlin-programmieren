class Konto(val ktoNummer:String, val ktoInhaber:String) {

    private var ktoStand = 0.0

    fun zahleEin(betrag:Double) {

        val eineVariable = ""
        if (betrag > 0) {
            ktoStand = betrag + ktoStand
        }
    }

    fun gibKtoStandAus():Double {

        return ktoStand
    }

    fun zahleAus(betrag:Double) {
        if (betrag <= ktoStand) {
            ktoStand = ktoStand - betrag
        }
    }

}