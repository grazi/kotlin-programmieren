abstract class Koch(val name:String) {

    val rezeptBuch = RezeptBuch()

    fun koche(rezeptName:String) {
        for (rezept in rezeptBuch.rezepte) {
            if (rezeptName == rezept.name) {
                println("Ich koche nun dieses Rezept $rezept")
                println("Das Gericht ist gekocht und kann serviert werden...")
                Service.serviereGericht()
            }
        }
    }

    open fun lerneRezept(rezeptName: String) : String {
        rezeptBuch.fuegeNeuesRezeptHinzu(rezeptName)
        return "Ich kann nun neu das Rezept: $rezeptName kochen"
    }

}
