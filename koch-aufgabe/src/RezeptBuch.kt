class RezeptBuch {

    val rezepte = arrayListOf<Rezept>()


    fun fuegeNeuesRezeptHinzu(rezeptName:String) {
        rezepte.add(Rezept(rezeptName, "..."))
    }

    fun sucheRezept(rezeptName: String) :Rezept {
        for (rezept in rezepte) {
            if (rezept.name == rezeptName) {
                return rezept
            }
        }
        throw IllegalArgumentException("Kein Rezept gefunden!")
    }



}