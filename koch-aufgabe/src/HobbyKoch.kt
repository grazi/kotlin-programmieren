class HobbyKoch(name: String) : Koch(name) {

    override fun lerneRezept(rezeptName: String): String {
        rezeptBuch.fuegeNeuesRezeptHinzu(rezeptName)
        return "Ich kann nun das Rezept $rezeptName einigermassen kochen..."
    }

}