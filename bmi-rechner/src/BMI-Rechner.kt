fun main(args: Array<String>) {

    val geschlecht = frageTextAb("Gib bitte Dein Geschlecht an: (m oder w)")

    if (istEingegebenesGeschlechtKorrekt(geschlecht)) {
        val koerperGewicht = frageZahlAb("Gib bitte Dein Körpergewicht an:")
        val koerperGroesse = frageFliessKomaZahlAb("Gib bitte Deine Körpergrösse an:")

        val bmi: Double = berechneBMI(koerperGewicht, koerperGroesse)

        gibBewertungAus(geschlecht, bmi)
    } else {
        gibFehlermeldungAus(geschlecht)
    }

}

fun istEingegebenesGeschlechtKorrekt(geschlecht: String):Boolean {
    return geschlecht == "m" || geschlecht == "w"
}

fun gibFehlermeldungAus(geschlecht: String) {
    println("Sorry, ich kenne das Geschlecht: $geschlecht nicht!")
}

fun gibBewertungAus(geschlecht: String, bmi: Double) {

    val bewertung = ermittleBMI(geschlecht, bmi)

    println("Dein BMI Index = $bmi")
    println("Dieser Index bedeutet:")
    println(bewertung)

}

fun ermittleBMI(geschlecht: String, bmi: Double): String {

    val bmiBewertung:String
    if (geschlecht == "m") {
        if (bmi < 19) {
            bmiBewertung = "Untergewicht"
        } else if (bmi < 25) {
            bmiBewertung = "Normalgewicht"
        } else if (bmi < 31) {
            bmiBewertung = "leichtes bis mittleres Übergewicht"
        } else if (bmi < 41) {
            bmiBewertung = "schweres Übergewicht"
        } else {
            bmiBewertung = "massiv gefährdendes Übergewicht"
        }
    } else {
        if (bmi < 20) {
            bmiBewertung = "Untergewicht"
        } else if (bmi < 26) {
            bmiBewertung = "Normalgewicht"
        } else if (bmi < 31) {
            bmiBewertung = "leichtes bis mittleres Übergewicht"
        } else if (bmi < 41) {
            bmiBewertung = "schweres Übergewicht"
        } else {
            bmiBewertung = "massiv gefährdendes Übergewicht"
        }
    }

    return bmiBewertung
}

fun berechneBMI(koerperGewicht: Int, koerperGroesse: Double): Double {

    if (koerperGewicht == 0) {
        return 0.0
    }

    return koerperGewicht / (koerperGroesse * koerperGroesse)
}

fun frageTextAb(frage: String): String {

    println(frage)

    val text = readLine()

    if (text == null) {
        return ""
    }

    return text
}

fun frageZahlAb(frage: String): Int {

    println(frage)

    val zahl = readLine()?.toIntOrNull()

    if (zahl == null) {
        return 0
    }

    return zahl
}

fun frageFliessKomaZahlAb(frage: String): Double {

    println(frage)

    val fliessKommaZahl = readLine()?.toDoubleOrNull()

    if (fliessKommaZahl == null) {
        return 0.0
    }

    return fliessKommaZahl
}