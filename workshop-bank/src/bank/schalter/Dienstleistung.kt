package bank.schalter

enum class Dienstleistung {

    Einzahlen, KontostandAbfragen, KontoEroeffnen

}