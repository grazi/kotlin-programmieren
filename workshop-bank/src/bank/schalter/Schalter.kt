package bank.schalter

import bank.Bank
import bank.kunde.konto.KontoTyp
import bank.schalter.Dienstleistung.*

class Schalter(val bank: Bank) {

    fun geheInBetrieb() {

        while (true) {

            kundeBegruessen()
            val kundenNummer = kundennummerErmitteln()
            //val kontoNummer = kontonummerErmitteln()
            val dienstleistung = gewuenschteDienstleistungErmitteln()
            when (dienstleistung) {
                KontoEroeffnen -> {

                }
                Einzahlen -> {
                    val betrag = betragErmitteln()
                    bank.zahleEin(kundenNummer, betrag)
                }
            }
            kundeVerabschieden()

        }

    }

    private fun kundeVerabschieden() {
        println("""
            |
            |
            |Wir wünschen Ihnen einen wunderschönen restlichen Tag
            | Aufwiedersehen.
            |
            |
        """.trimMargin())
    }

    private fun betragErmitteln(): Double {
        return liesTextEin("Welchen Betrag möchten Sie einzahlen?").toDouble()
    }

    private fun gewuenschteDienstleistungErmitteln(): Dienstleistung {
        val antwort = liesTextEin("Welche Dienstleistung wünschen Sie? (e) einzahlen | (k) Kontostand abfragen)")
        when (antwort) {
            "e" -> return Einzahlen
            "k" -> return KontostandAbfragen
            else -> throw IllegalStateException("""Es tut mir leid, diese Dienstleistung bieten wir nicht an!
                | Auf wiedersehen..
            """.trimMargin())
        }
    }
    /*
        private fun kontonummerErmitteln(): String {
            if (bereitsEinKontoVorhanden()) {
                return frageKontonummerAb()
            }
            //val kontoTyp = kontoTypAbfragen()
            return eroeffneKontoNummer()
        }

        private fun kontoTypAbfragen(): KontoTyp {
            val antwort = liesTextEin("Wünschen Sie ein Giro- (g) oder ein Sparkonto (s)")

            when (antwort) {
                "g" -> return bank.kunde.konto.KontoTyp.SparKonto
            }
        }

        private fun eroeffneKontoNummer(kundenNummer:String, kontoTyp: KontoTyp): String {
            val kontoNummer = bank.eroeffneKonto(kundenNummer, kontoTyp)
            println("""Vielen Dank
                |Ihre Kontonummer bei uns lautet $kontoNummer
            """.trimMargin())
            return kontoNummer
        }
    */
    private fun frageKontonummerAb(): String {
        return liesTextEin("Wie lautet Ihre Kontonummer?")
    }

    private fun bereitsEinKontoVorhanden(): Boolean {
        val antwort = liesTextEin("""Haben Sie bereits ein Konto bei uns?
            | (ja oder nein)
        """.trimMargin())

        return antwort == "ja"
    }

    private fun kundennummerErmitteln(): String {
        if (bereitsKunde()) {
            return frageKundennummerAb()
        }
        return registriereKunde()

    }

    private fun frageKundennummerAb(): String {
        return liesTextEin("Wie lautet Ihre Kundennummer?")
    }

    private fun registriereKunde(): String {
        val name = liesTextEin("Wie lautet ihr Name?")
        val kdNr = bank.registriereKunde(name)
        println("""Vielen Dank
            |Ihre Kundennummer bei uns lautet $kdNr
        """.trimMargin())
        return kdNr
    }


    private fun kundeBegruessen() {
        println("Herzlich willkommen bei der ${bank.name} Bank.")
    }

    private fun bereitsKunde(): Boolean {
        val antwort = liesTextEin("""Sind Sie bei uns ein Kunde?
            | (ja oder nein)
        """.trimMargin())

        return antwort == "ja"
    }

    private fun liesTextEin(frage: String): String {

        var text: String? = null
        do {
            println(frage)
            text = readLine()
        } while (text == null)

        return text
    }


}