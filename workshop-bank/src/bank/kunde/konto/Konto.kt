package bank.kunde.konto

import bank.generator.KontoNummerGenerator

abstract class Konto(private val ktoNummer:String, private val kdNr:String) {

    private var ktoStand = 0.0


    abstract fun zahleEin(betrag:Double);

    fun gibKtoStandAus() {
        println("KtoStand: $ktoStand")
    }

}