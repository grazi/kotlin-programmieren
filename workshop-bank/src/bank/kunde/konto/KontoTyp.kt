package bank.kunde.konto

enum class KontoTyp {

    GiroKonto, SparKonto

}