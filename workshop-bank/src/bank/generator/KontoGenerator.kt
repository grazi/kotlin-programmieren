package bank.generator

import bank.kunde.konto.GiroKonto
import bank.kunde.konto.Konto
import bank.kunde.konto.KontoTyp
import bank.kunde.konto.Sparkonto

class KontoGenerator {

    private val ktoNrGenerator = KontoNummerGenerator()

    fun erstelleKonto(kdNr: String, ktoTyp: KontoTyp): Konto {

        if (ktoTyp == KontoTyp.GiroKonto) {
            return GiroKonto(ktoNrGenerator.generiereKtoNr(), kdNr)
        }

        if (ktoTyp == KontoTyp.SparKonto) {
            return Sparkonto(ktoNrGenerator.generiereKtoNr(), kdNr)
        }

        throw IllegalStateException("Sorry, diesen Kontotyp $ktoTyp kenne ich nicht!")
    }


}