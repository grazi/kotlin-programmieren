package bank

import bank.generator.KontoGenerator
import bank.generator.KundenNummerGenerator
import bank.kunde.Kunde
import bank.kunde.konto.Konto
import bank.kunde.konto.KontoTyp
import bank.schalter.Schalter

class Bank(val name: String) {


    val kunden = arrayListOf<Kunde>()
    val kontos = arrayListOf<Konto>()

    val kundenNummerGenerator = KundenNummerGenerator()
    val kontoGenerator = KontoGenerator()


    fun registriereKunde(kundenName: String) : String {
        val kdNr = kundenNummerGenerator.generiereKdNr()
        kunden.add(Kunde(kdNr, name))
        return kdNr
    }

    fun eroeffneKonto(kdNr:String, ktoTyp: KontoTyp) {
        kontos.add(kontoGenerator.erstelleKonto(kdNr, ktoTyp))

    }


    fun zahleEin(ktoNr:String, betrag:Double)  {

    }

    fun geheOnline() {
        Schalter(this).geheInBetrieb()
    }
}