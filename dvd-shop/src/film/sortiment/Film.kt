package film.sortiment

data class Film(val titel:String,
           val genre:Genre,
           var preis:Double,
           var istVerfuegbar:Boolean = true) {


    fun duBistVermietet() {
        istVerfuegbar = false
    }


}