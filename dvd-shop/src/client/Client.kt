package client

import film.sortiment.Film
import film.sortiment.Genre
import shop.FilmShop


fun main(args: Array<String>) {

    val starWars = Film("Star Wars", Genre.ACTION, 10.0, false)

    val starTrek = Film("Star Trek", Genre.SCIFI, 3.00, true)

    val rambo = Film("Rambo", Genre.ACTION, 5.00)

    val kopieStarWars = starWars.copy(genre = Genre.HORROR)

    val samsShop = FilmShop(arrayListOf(
            kopieStarWars,
            starTrek.copy(),
            rambo.copy()))

    val maxiShop = FilmShop(arrayListOf(starWars, starTrek, rambo))


    println(kopieStarWars)

}